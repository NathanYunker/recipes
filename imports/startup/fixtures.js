import { Categories } from '../api/categories/index';
import { Meteor } from 'meteor/meteor';
import { Metrics } from '../api/metrics/index';
import { Recipes } from '../api/recipes/index';

Meteor.startup(() => {
  if (Recipes.find().count() === 0) {
    const recipes = [
    {
      ingredients:[
        {
          measure:"4",
          name:"Slices Bread"
        },
        {
          measure:"4",
          name:"Slices Cheese"
        },
        {
          measure:"4",
          metric:"tablespoon",
          name:"butter"
        }
      ],
      directions: [
        {
          direction:"Place two pieces of cheese between two slices of bread, while melting 4 tbsp butter in a pan."
        },
        {
          direction:"Place the bread and cheese in the pan and grill on one side until golden brown, flip to the other side and repeat."
        }
      ],
      public:true,
      name:"Grilled Cheese",
      numberServed:"2",
      prepTime:"10 mins",
      totalTime:"10 mins",
      numberOfDishes:1,
      cost:1,
      rating:4,
      primaryCategories:[
        {
          name:"Misc",
          primacy:"primary",
          _id:"EXyNAP8Y9ee6gf323"
        },
        {
          name:"Lunch",
          primacy:"primary",
          _id:"dvZGhaiMBLmDN2sBo"
        },
        {
          name:"Appetizer",
          primacy:"primary",
          _id:"2f3tswgE9tzzH9LRQ"
        }
      ],
      secondaryCategories:[
        {
          name:"Vegitarian",
          primacy:"secondary",
          _id:"v6PzzPMfR6FKdMhcn"
        }
      ],
      description:"A delicious classic. Easy to make and very delicious.",
      owner:"F6kHxnicPas5Xq8EB",
      recipeId:"61ac8d115aa37df2e50cec82",
      _id:"MX3miyPikfuHSZZFE"
    },
    {
      ingredients:[
        {
          measure:"1",
          metric:"cup",
          name:"Flour"
        },
        {
          measure:"2",
          metric:"tablespoon",
          name:"Sugar"
        },
        {
          measure:"2",
          metric:"teaspoon",
          name:"Baking Powder"
        },
        {
          measure:"1",
          metric:"teaspoon",
          name:"Salt"
        },
        {
          measure:"1",
          name:"Egg"
        },
        {
          measure:"1",
          metric:"cup",
          name:"Milk"
        },
        {
          measure:"2",
          metric:"tablespoon",
          name:"Vegetable Oil"
        }
      ],
      directions:[
        {
          direction:"In a large bowl, mix flour, sugar, baking powder and salt. Make a well in the center, and pour in milk, egg and oil. Mix until smooth."
        },
        {
          direction:"Heat a lightly oiled griddle or frying pan over medium high heat. Pour or scoop the batter onto the griddle, using approximately 1/4 cup for each pancake. Brown on both sides and serve hot."
        }
      ],
      public:true,
      name:"Pancakes",
      numberServed:"2",
      numberOfDishes:3,
      cost:2,
      rating:4,
      prepTime:"20",
      totalTime:"20",
      primaryCategories:[
        {
          name:"Breakfast",
          primacy:"primary",
          _id:"BhBFiKWemiwLjiE3L"
        },
        {
          name:"Dessert",
          primacy:"primary",
          _id:"C9eJ3fsfLa8qrYWXQ"
        }
      ],
      secondaryCategories: [
        {
          name:"Vegitarian",
          primacy:"secondary",
          _id:"v6PzzPMfR6FKdMhcn"
        }
      ],
      description:"A classic breakfast staple.",
      owner:"F6kHxnicPas5Xq8EB",
      recipeId:"d75dbb99a86fbc3d22b43083",
      _id:"JxKyuWehGejGqRRNF"
    }
  ];

    recipes.forEach((recipe) => {
      Recipes.insert(recipe)
    });
  }

  if (Categories.find().count() === 0) {

    const categories = [
      {name: 'Breakfast', primacy: 'primary'}, 
      {name: 'Lunch', primacy: 'primary'}, 
      {name: 'Beverage', primacy: 'primary'}, 
      {name: 'Appetizer', primacy: 'primary'}, 
      {name: 'Seafood', primacy: 'primary'}, 
      {name: 'Canning/Preserving', primacy: 'primary'}, 
      {name: 'Holiday', primacy: 'primary'}, 
      {name: 'Pizza', primacy: 'primary'}, 
      {name: 'Misc', primacy: 'primary'}, 
      {name: 'Soup', primacy: 'primary'}, 
      {name: 'Salad', primacy: 'primary'}, 
      {name: 'Beef', primacy: 'primary'}, 
      {name: 'Pork', primacy: 'primary'}, 
      {name: 'Dessert', primacy: 'primary'}, 
      {name: 'Breads', primacy: 'primary'}, 
      {name: 'Entertaining', primacy: 'primary'}, 
      {name: 'Grilling', primacy: 'primary'}, 
      {name: 'Sauces/Dressing/Gravy', primacy: 'primary'}, 
      {name: 'Alcoholic', primacy: 'secondary'},
      {name: 'Gluten Free', primacy: 'secondary'},
      {name: 'Dairy Free', primacy: 'secondary'},
      {name: 'Vegitarian', primacy: 'secondary'},
      {name: 'Vegan', primacy: 'secondary'},
      {name: 'Crockpot', primacy: 'secondary'},
      {name: 'No Cook', primacy: 'secondary'}
    ]

    categories.forEach((category) => {
      Categories.insert(category)
    });
  }

  if (Metrics.find().count() === 0) {
    
    
    const metrics = [
      { 
        name: 'teaspoon', 
        abbr: 'tsp', 
        conversion: {toTablespoon: .3} 
      }, 
      { 
        name: 'tablespoon', 
        abbr: 'tbsp', 
        conversion: {toTeaspoon: 3, toCup: .125} 
      }, 
      { 
        name: 'cup', 
        conversion: {toPint: .5, toTablespoon: 16} 
      }, 
      {
        name: 'ounce', 
        abbr: 'oz', 
        conversion: {toGram: 28}
      }
    ]


    metrics.forEach((metrics) => {
      Metrics.insert(metrics)
    });
  }
});
