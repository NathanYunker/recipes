import { Mongo } from 'meteor/mongo';

export const Metrics = new Mongo.Collection('metrics');

Metrics.allow({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  }
});
