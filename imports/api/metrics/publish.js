import { Meteor } from 'meteor/meteor';
import { Counts } from 'meteor/tmeasday:publish-counts';

import { Metrics } from './collection';

if (Meteor.isServer) {
  Meteor.publish('metrics', function () {
    return Metrics.find();
  });
}
