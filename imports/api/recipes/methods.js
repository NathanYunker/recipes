import _ from 'underscore';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Email } from 'meteor/email';

import { Recipes } from './collection';

function getContactEmail(user) {
  if (user.emails && user.emails.length)
    return user.emails[0].address;

  if (user.services && user.services.facebook && user.services.facebook.email)
    return user.services.facebook.email;

  return null;
}

export function share(recipeId, userId) {
  check(recipeId, String);
  check(userId, String);

  if (!this.userId) {
    throw new Meteor.Error(400, 'You have to be logged in!');
  }

  const recipe = Recipes.findOne(recipeId);

  if (!recipe) {
    throw new Meteor.Error(404, 'No such recipe!');
  }

  if (recipe.owner !== this.userId) {
    throw new Meteor.Error(404, 'No permissions!');
  }

  if (recipe.public) {
    throw new Meteor.Error(400, 'That recipe is public. No need to share with people.');
  }

  if (userId !== recipe.owner && !_.contains(recipe.shared, userId)) {
    Recipes.update(recipeId, {
      $addToSet: {
        shared: userId
      }
    });

    const replyTo = getContactEmail(Meteor.users.findOne(this.userId));
    const to = getContactEmail(Meteor.users.findOne(userId));

    if (Meteor.isServer && to) {
      Email.send({
        to,
        replyTo,
        from: 'noreply@reciplease.com',
        subject: `RECIPE: ${recipe.title}`,
        text: `
          Hey, I just shared ${recipe.title} with you on Reciplease.
          Come check it out: ${Meteor.absoluteUrl()}
        `
      });
    }
  }
}

Meteor.methods({
  share
});
