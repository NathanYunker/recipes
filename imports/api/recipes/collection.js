import { Mongo } from 'meteor/mongo';

export const Recipes = new Mongo.Collection('recipes');

Recipes.allow({
  insert(userId, recipe) {
    return userId && recipe.owner === userId;
  },
  update(userId, recipe, fields, modifier) {
    return userId && recipe.owner === userId;
  },
  remove(userId, recipe) {
    return userId && recipe.owner === userId;
  }
});
