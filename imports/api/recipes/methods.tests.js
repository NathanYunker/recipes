import { share } from './methods';
import { Recipes } from './collection';

import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
  describe('Recipes / Methods', () => {
    describe('share', () => {
      function loggedIn(userId = 'userId') {
        return {
          userId
        };
      }

      it('should be called from Method', () => {
        spyOn(share, 'apply');

        try {
          Meteor.call('share');
        } catch (e) {}

        expect(share.apply).toHaveBeenCalled();
      });

      it('should fail on missing recipeId', () => {
        expect(() => {
          share.call({});
        }).toThrowError();
      });

      it('should fail on missing userId', () => {
        expect(() => {
          share.call({}, 'recipeId');
        }).toThrowError();
      });

      it('should fail on not logged in', () => {
        expect(() => {
          share.call({}, 'recipeId', 'userId');
        }).toThrowError(/logged in/i);
      });

      it('should look for a recipe', () => {
        const recipeId = 'recipeId';
        spyOn(Recipes, 'findOne');

        try {
          share.call(loggedIn(), recipeId, 'userId');
        } catch (e) {}

        expect(Recipes.findOne).toHaveBeenCalledWith(recipeId);
      });

      it('should fail if recipe does not exist', () => {
        spyOn(Recipes, 'findOne').and.returnValue(undefined);

        expect(() => {
          share.call(loggedIn(), 'recipeId', 'userId');
        }).toThrowError(/404/);
      });

      it('should fail if logged in user is not the owner', () => {
        spyOn(Recipes, 'findOne').and.returnValue({
          owner: 'notUserId'
        });

        expect(() => {
          share.call(loggedIn(), 'recipeId', 'userId');
        }).toThrowError(/404/);
      });

      it('should fail on public recipe', () => {
        spyOn(Recipes, 'findOne').and.returnValue({
          owner: 'userId',
          public: true
        });

        expect(() => {
          share.call(loggedIn(), 'recipeId', 'userId');
        }).toThrowError(/400/);
      });

      it('should NOT share user who is the owner', () => {
        spyOn(Recipes, 'findOne').and.returnValue({
          owner: 'userId'
        });
        spyOn(Recipes, 'update');

        share.call(loggedIn(), 'recipeId', 'userId');

        expect(Recipes.update).not.toHaveBeenCalled();
      });

      it('should NOT share user who has been already shared', () => {
        spyOn(Recipes, 'findOne').and.returnValue({
          owner: 'userId',
          shared: ['sharedId']
        });
        spyOn(Recipes, 'update');

        share.call(loggedIn(), 'recipeId', 'sharedId');

        expect(Recipes.update).not.toHaveBeenCalled();
      });

      it('should share user who has not been shared and is not the owner', () => {
        const recipeId = 'recipeId';
        const userId = 'notsharedId';
        spyOn(Recipes, 'findOne').and.returnValue({
          owner: 'userId',
          shared: ['sharedId']
        });
        spyOn(Recipes, 'update');
        spyOn(Meteor.users, 'findOne').and.returnValue({});

        share.call(loggedIn(), recipeId, userId);

        expect(Recipes.update).toHaveBeenCalledWith(recipeId, {
          $addToSet: {
            shared: userId
          }
        });
      });
    });
  });
}
