import { Meteor } from 'meteor/meteor';
import { Counts } from 'meteor/tmeasday:publish-counts';

import { Categories } from './collection';

if (Meteor.isServer) {
  Meteor.publish('categories', function () {
    return Categories.find();
  });
}
