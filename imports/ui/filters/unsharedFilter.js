import angular from 'angular';
import _ from 'underscore';

const name = 'unsharedFilter';

function UnsharedFilter(users, recipe) {
  if (!recipe) {
    return false;
  }

  return users.filter((user) => {
    // if not the owner and not shared
    return user._id !== recipe.owner && !_.contains(recipe.shared, user._id);
  });
}

// create a module
export default angular.module(name, [])
  .filter(name, () => {
    return UnsharedFilter;
  });
