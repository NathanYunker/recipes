import angular from 'angular';
import angularMeteor from 'angular-meteor';

import template from './recipeRemove.html';
import { Recipes } from '../../../api/recipes/index';
import { Images } from '../../../api/images';

class RecipeRemove {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);

    this.subscribe('recipes');
    this.subscribe('images');

    this.helpers({
      image() {
        return Images.findOne({recipeId: this.recipe.recipeId})
      }
    });
  }

  remove() {
    if (this.recipe) {
      Recipes.remove(this.recipe._id);
      Images.remove(this.image._id);
    }
  }
}

const name = 'recipeRemove';

// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  template,
  bindings: {
    recipe: '<'
  },
  controllerAs: name,
  controller: RecipeRemove
});
