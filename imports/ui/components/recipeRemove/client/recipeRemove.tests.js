import { name as RecipeRemove } from '../recipeRemove';
import { Recipes } from '../../../../api/recipes';
import 'angular-mocks';

describe('RecipeRemove', () => {
  beforeEach(() => {
    window.module(RecipeRemove);
  });

  describe('controller', () => {
    let controller;
    const recipe = {
      _id: 'recipeId'
    };

    beforeEach(() => {
      inject(($rootScope, $componentController) => {
        controller = $componentController(RecipeRemove, {
          $scope: $rootScope.$new(true)
        }, {
          recipe
        });
      });
    });

    describe('remove()', () => {
      beforeEach(() => {
        spyOn(Recipes, 'remove');
        controller.remove();
      });

      it('should remove a recipe', () => {
        expect(Recipes.remove).toHaveBeenCalledWith(recipe._id);
      });
    });
  });
});
