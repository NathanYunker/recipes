import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import utilsPagination from 'angular-utils-pagination';

import { Counts } from 'meteor/tmeasday:publish-counts';

import template from './recipesList.html';
import { Recipes } from '../../../api/recipes/index';
import { Categories } from '../../../api/categories/index';
import { name as RecipesSort } from '../recipesSort/recipesSort';
import { name as RecipeAdd } from '../recipeAdd/recipeAdd';
import { name as RecipeRemove } from '../recipeRemove/recipeRemove';
import { name as RecipeCreator } from '../recipeCreator/recipeCreator';

class RecipesList {
  constructor($scope, $reactive, $state) {
    'ngInject';

    $reactive(this).attach($scope);

    this.perPage = 3;
    this.page = 1;
    this.sort = {
      name: 1
    };
    this.searchText = '';

    this.subscribe('recipes', () => [{
      limit: parseInt(this.perPage),
      skip: parseInt((this.getReactively('page') - 1) * this.perPage),
      sort: this.getReactively('sort')
    }, this.getReactively('searchText')]);

    this.subscribe('categories');

    this.helpers({
      recipes() {
        return Recipes.find({}, {
          sort: this.getReactively('sort')
        });
      },
      categories() {
        return Categories.find({})
      },
      recipesCount() {
        return Counts.get('numberOfRecipes');
      },
      isLoggedIn() {
        return !!Meteor.userId();
      },
      currentUserId() {
        return Meteor.userId();
      }
    });

    this.goToAddRecipe = function() {
      $state.go('recipeAdd');
    }
  }

  isOwner(recipe) {
    return this.isLoggedIn && recipe.owner === this.currentUserId;
  }

  pageChanged(newPage) {
    this.page = newPage;
  }

  sortChanged(sort) {
    this.sort = sort;
  }
}

const name = 'recipesList';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  utilsPagination,
  RecipesSort,
  RecipeAdd,
  RecipeRemove,
  RecipeCreator
])
.component(name, {
  template,
  controllerAs: name,
  controller: RecipesList
})
.config(config);

function config($stateProvider) {
  'ngInject';
  $stateProvider
    .state('recipes', {
      url: '/recipes',
      template: '<recipes-list></recipes-list>'
    });
}

