import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';

import template from './recipeAdd.html';
import { Recipes } from '../../../api/recipes/index';
import { Categories } from '../../../api/categories/index';
import { name as ImageUpload } from '../imageUpload/imageUpload';

class RecipeAdd {
  constructor($scope, $reactive, $state, $timeout) {
    'ngInject';

    $reactive(this).attach($scope);

    this.recipeId = new Meteor.Collection.ObjectID()._str;

    this.subscribe('recipes');

    this.subscribe('categories');

    this.helpers({
      primaryCategories() {
        return Categories.find({primacy: 'primary'})
      },
      secondaryCategories() {
        return Categories.find({primacy: 'secondary'})
      }
    });

    this.recipe = {};
    this.recipe.ingredients = [];
    this.recipe.directions = [];
    this.ingredientInputs = [];
    this.directionInputs = [];
    this.recipe.public = true;

    this.possibleMetrics = [{ name: 'teaspoon', abbr: 'tsp', conversion: {toTablespoon: .3} }, { name: 'tablespoon', abbr: 'tbsp', conversion: {toTeaspoon: 3, toCup: .125} }, { name: 'cup', conversion: {toPint: .5, toTablespoon: 16} }, {name: 'ounce', abbr: 'oz', conversion: {toGram: 28}}]

    for (var i = 0; i < 5; i++) {
      this.ingredientInputs.push({ index: i });
    }

    for (var d = 0; d < 3; d++) {
      this.directionInputs.push({ index: d });
    }

    this.addRecipe = function() {
      this.recipe.owner = Meteor.user()._id;
      this.recipe.recipeId = this.recipeId;
      this.recipe.primaryCategories = angular.toJson( this.recipe.primaryCategories );
      this.recipe.secondaryCategories = angular.toJson( this.recipe.secondaryCategories );
      Recipes.insert(this.recipe);
      $state.go('recipeDetails', {recipeId: this.recipe.recipeId})
      this.reset();
    }
  }

  addDirection() {
    var newDirection = {
      index: this.recipe.directions.length,
      text: this.currentDirection
    };
    this.recipe.directions.push(newDirection)
    this.currentDirection = '';
  }

  addOrRemoveCategory(category) {
    if(_.some(this.recipe.categories, {name: category.name})) {
      _.remove(this.recipe.categories, function(recipeCategory) {
        return recipeCategory.name === category.name;
      });
    } else {
      this.recipe.categories.push(category);
    }
  }

  incrementDirectionInputs() {
    for (var i = 1; i < 4; i++) {
      this.directionInputs.push({ index: i + (_.get(_.maxBy(this.directionInputs, 'index'), 'index') || this.directionInputs.length)});
    }
  }

  incrementIngredientInputs() {
    for (var i = 1; i < 6; i++) {
      this.ingredientInputs.push({ index: i + (_.get(_.maxBy(this.ingredientInputs, 'index'), 'index') || this.ingredientInputs.length)});
    }  
  }

  removeDirection(index) {
    _.remove(this.directionInputs, function (ingredientInput) {
      return directionInput.index == index;
    });
  }

  removeIngredient(index) {
    _.remove(this.ingredientInputs, function (ingredientInput) {
      return ingredientInput.index == index;
    });
  }

  removeDirection(directionIndex) {
    _.remove(this.directionInputs, function (direction) {
      return direction.index == directionIndex;
    });
  }

  reset() {
    this.recipe = {};
  }
}

const name = 'recipeAdd';

// create a module
export default angular.module(name, [
  angularMeteor,
  ImageUpload
]).component(name, {
  template,
  controllerAs: name,
  controller: RecipeAdd
})
.config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider.state('recipeAdd', {
    url: '/recipeAdd',
    template: '<recipe-add></recipe-add>'
  });
}
