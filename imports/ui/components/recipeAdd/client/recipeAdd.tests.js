import { Meteor } from 'meteor/meteor';

import { name as RecipeAdd } from '../recipeAdd';
import { Recipes } from '../../../../api/recipes';
import 'angular-mocks';

describe('RecipeAdd', () => {
  beforeEach(() => {
    window.module(RecipeAdd);
  });

  describe('controller', () => {
    let controller;
    const recipe = {
      name: 'Foo',
      description: 'Foo recipe',
      public: true
    };
    const user = {
      _id: 'userId'
    };

    beforeEach(() => {
      inject(($rootScope, $componentController) => {
        controller = $componentController(RecipeAdd, {
          $scope: $rootScope.$new(true)
        });
      });

      spyOn(Meteor, 'user').and.returnValue(user);
    });

    describe('reset()', () => {
      it('should clean up recipe object', () => {
        controller.recipe = recipe;
        controller.reset();

        expect(controller.recipe).toEqual({});
      });
    });

    describe('addRecipe()', () => {
      beforeEach(() => {
        spyOn(Recipes, 'insert');
        spyOn(controller, 'reset').and.callThrough();

        controller.recipe = recipe;

        controller.addRecipe();
      });

      it('should insert a new recipe', () => {
        expect(Recipes.insert).toHaveBeenCalledWith({
          name: recipe.name,
          description: recipe.description,
          public: recipe.public,
          owner: user._id
        });
      });

      it('should call reset()', () => {
        expect(controller.reset).toHaveBeenCalled();
      });
    });
  });
});
