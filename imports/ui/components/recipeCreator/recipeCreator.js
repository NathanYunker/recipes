import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';

import template from './recipeCreator.html';
import { name as DisplayNameFilter } from '../../filters/displayNameFilter';

/**
 * RecipeCreator component
 */
class RecipeCreator {
  constructor($scope) {
    'ngInject';

    $scope.viewModel(this);

    this.helpers({
      creator() {
        if (!this.recipe) {
          return '';
        }

        const owner = this.recipe.owner;

        if (Meteor.userId() !== null && owner === Meteor.userId()) {
          return 'me';
        }


        return Meteor.users.findOne(owner) || 'nobody';
      }
    });
  }
}

const name = 'recipeCreator';

// create a module
export default angular.module(name, [
  angularMeteor,
  DisplayNameFilter
]).component(name, {
  template,
  controllerAs: name,
  bindings: {
    recipe: '<'
  },
  controller: RecipeCreator
});
