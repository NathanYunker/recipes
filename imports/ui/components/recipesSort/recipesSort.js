import angular from 'angular';
import anuglarMeteor from 'angular-meteor';

import template from './recipesSort.html';

class RecipesSort {
  constructor() {
    this.changed();
  }

  changed() {
    this.onChange({
      sort: {
        [this.property]: parseInt(this.order)
      }
    });
  }
}

const name = 'recipesSort';

//create a module

export default angular.module(name, [
  anuglarMeteor
]).component(name, {
  template,
  bindings: {
    onChange: '&',
    property: '@',
    order: '@'
  },
  controllerAs: name,
  controller: RecipesSort
});
