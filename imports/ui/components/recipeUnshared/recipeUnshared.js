import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';

import template from './recipeUnshared.html';
import { name as UnsharedFilter } from '../../filters/unsharedFilter';
import { name as DisplayNameFilter } from '../../filters/displayNameFilter';

class RecipeUnshared {
  constructor($scope) {
    'ngInject';

    $scope.viewModel(this);

    this.helpers({
      users() {
        return Meteor.users.find({});
      }
    });
  }

  share(user) {
    Meteor.call('share', this.recipe._id, user._id,
      (error) => {
        if (error) {
          console.log('Oops, unable to share!');
        } else {
          console.log('Shared!');
        }
      }
    );
  }
}

const name = 'recipeUnshared';

// create a module
export default angular.module(name, [
  angularMeteor,
  UnsharedFilter,
  DisplayNameFilter
]).component(name, {
  template,
  controllerAs: name,
  bindings: {
    recipe: '<'
  },
  controller: RecipeUnshared
});
