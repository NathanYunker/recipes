import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngFileUpload from 'ng-file-upload';
import 'ng-img-crop/compile/minified/ng-img-crop';
import 'ng-img-crop/compile/minified/ng-img-crop.css';
 
import { Meteor } from 'meteor/meteor';
 
import template from './imageUpload.html';
import { Thumbs, Images, upload } from '../../../api/images';
 
class ImageUpload {
  constructor($scope, $reactive) {
    'ngInject';
 
    $reactive(this).attach($scope);

    this.subscribe('images');

    this.uploaded = [];

    //SUBSCRIBED TO THUMBS DATABASE

    // this.subscribe('thumbs', () => [
    //   this.getReactively('files', true) || []
    // ]);
 
    this.helpers({
    //   thumbs() {
    //     return Thumbs.find({
    //       originalStore: 'images',
    //       originalId: {
    //         $in: this.getReactively('files', true) || []
    //       }
    //     });
    //   },
      recipeImage() {
        return Images.findOne({recipeId: this.recipeId});
      }
    });
  }
 
  addImages(files) {
    if (files.length) {
      this.currentFile = files[0];
 
      const reader = new FileReader;
 
      reader.onload = this.$bindToContext((e) => {
        this.cropImgSrc = e.target.result;
        this.myCroppedImage = '';
      });
 
      reader.readAsDataURL(files[0]);
    } else {
      this.cropImgSrc = undefined;
    }
  }

  save(event, args) {
    upload(this.recipeId, this.cropImgSrc, this.currentFile.name, this.$bindToContext((file) => {
      this.uploaded.push(file);
      // this.reset();

      if(this.originalImageId) {
        Images.remove({_id: this.originalImageId});
      }

    }), (e) => {
      console.log('Oops, something went wrong', e);
    });
  }
 
  reset() {
    this.originalImageId = _.get(this.recipeImage, '_id');
    this.cropImgSrc = undefined;
    this.recipeImage =undefined;
    this.myCroppedImage = '';
  }
}
 
const name = 'imageUpload';
 
// create a module
export default angular.module(name, [
  angularMeteor,
  ngFileUpload,
  'ngImgCrop'
]).component(name, {
  template,
  controllerAs: name,
  bindings: {
    recipeId: '=',
    editEnabled: '<'
  },
  controller: ImageUpload
});