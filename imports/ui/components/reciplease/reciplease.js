import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngSanitize from 'angular-sanitize';

import template from './reciplease.html';
import { name as RecipesList } from '../recipesList/recipesList';
import { name as RecipeDetails } from '../recipeDetails/recipeDetails';
import { name as Navigation } from '../navigation/navigation';

class Reciplease {}

const name = 'reciplease';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter,
    RecipesList,
    RecipeDetails,
    Navigation,
    'accounts.ui',
    'ui.select', 
    ngSanitize,
    uiBootstrap

  ]).component(name, {
    template,
    controllerAs: name,
    controller: Reciplease
  })
  .config(config)
  .run(run);

function config($locationProvider, $urlRouterProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/recipes');
}

function run($rootScope, $state) {
  'ngInject';

  $rootScope.$on('$stateChangeError',
    (event, toState, toParams, fromState, fromParams, error) => {
      if (error === 'AUTH_REQUIRED') {
        $state.go('recipes');
      }
    }
  );
}
