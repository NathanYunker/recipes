import { name as RecipeDetails } from '../recipeDetails';
import { Recipes } from '../../../../api/recipes';
import 'angular-mocks';

describe('RecipeDetails', () => {
  beforeEach(() => {
    window.module('ui.router')
    window.module(RecipeDetails);
  });

  describe('controller', () => {
    let controller;
    const recipe = {
      _id: 'recipeId',
      name: 'Foo',
      description: 'description',
      ingredients: 'ingredients',
      directions: 'directions',
      public: true
    };

    beforeEach(() => {
      inject(($rootScope, $componentController) => {
        controller = $componentController(RecipeDetails, {
          $scope: $rootScope.$new(true)
        });
      });
    });

    describe('save()', () => {
      beforeEach(() => {
        spyOn(Recipes, 'update');
        controller.recipe = recipe;
        controller.save();
      });

      it('should update a proper recipe', () => {
        expect(Recipes.update.calls.mostRecent().args[0]).toEqual({
          _id: recipe._id
        });
      });

      it('should update with proper modifier', () => {
        expect(Recipes.update.calls.mostRecent().args[1]).toEqual({
          $set: {
            name: recipe.name,
            description: recipe.description,
            ingredients: recipe.ingredients,
            directions: recipe.directions,
            public: recipe.public
          }
        })
      })
    })
  });
});
