import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { Recipes } from '../../../api/recipes/index';
import { name as ImageUpload } from '../imageUpload/imageUpload';

import { Meteor } from 'meteor/meteor';

import template from './recipeDetails.html';
import { name as RecipeUnshared } from '../recipeUnshared/recipeUnshared';
import { Images } from '../../../api/images/index';

class RecipeDetails {
  constructor($stateParams, $scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);

    this.recipeId = $stateParams.recipeId;

    this.subscribe('recipes');
    this.subscribe('images');

    this.helpers({
      image() {
        return Images.findOne({recipeId: $stateParams.recipeId});
      },
      isLoggedIn() {
        return !!Meteor.userId();
      },
      recipe() {
        return Recipes.findOne({recipeId: $stateParams.recipeId});
      },
      users() {
        return Meteor.users.find({});
      }
    });
  }

  canInvite() {
    if (!this.recipe) {
      return false;
    }

    return !this.recipe.public && this.recipe.owner === Meteor.userId();
  }

  save() {
    var updatedRecipe = angular.copy(this.recipe);
    delete updatedRecipe._id;

    if (this.recipeId) {
      Recipes.update({
        _id: this.recipe._id
      }, {
        $set: updatedRecipe
      }, (error) => {
        if (error) {
          console.log('Oops, unable to update the recipe...');
        } else {
          console.log('Done!');
        }
      });
    } else {
      console.log('this is where I will do the initial saving')
    }
  }
}

const name = 'recipeDetails';

// create a module
export default angular.module(name, [
    angularMeteor,
    ImageUpload,
    uiRouter,
    RecipeUnshared
  ]).component(name, {
    template,
    controllerAs: name,
    controller: RecipeDetails
  })
  .config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider.state('recipeDetails', {
    url: '/recipes/:recipeId',
    template: '<recipe-details></recipe-details>',
    params: {recipe: null},
    resolve: {
      currentUser($q) {
        if (Meteor.userId() === null) {
          return $q.reject('AUTH_REQUIRED');
        } else {
          return $q.resolve();
        }
      }
    }
  });
}
